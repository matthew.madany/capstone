WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 50, 50)
YELLOW = (255, 255, 0)
GREEN = (0, 255, 50)
BLUE = (50, 50, 255)
GREY = (200, 200, 200)
ORANGE = (255, 204, 153)
CYAN = (0, 255, 255)
MAGENTA = (255, 0, 255)
TRANS = (1, 1, 1)

PIXEL_RADIUS_DEFAULT = 2
PIXEL_RADIUS_MIN = 1
PIXEL_RADIUS_MAX = 20

class Slider():
    def __init__(self, pygame, screen, name, val, maxi, mini, pos, callback):
        self.pygame = pygame
        self.screen = screen
        self.width = 250
        self.padding = 10
        self.callback = callback
        self.val = val  # start value
        self.maxi = maxi  # maximum at slider position right
        self.mini = mini  # minimum at slider position left
        self.xpos = pos[0]  # x-location on screen
        self.ypos = pos[1]
        self.pos = pos
        self.surf = self.pygame.surface.Surface((self.width + (self.padding * 2), 50))
        self.hit = False  # the hit attribute indicates slider movement due to mouse interaction

        self.font = self.pygame.font.SysFont("Verdana", 14)
        self.txt_surf = self.font.render(name, 1, BLACK)
        self.txt_rect = self.txt_surf.get_rect(center=(50, 8))

        # Static graphics - slider background #
#         self.surf.fill((100, 100, 100))
#         pygame.draw.rect(self.surf, GREY, [0, 0, 100, 50], 3)
        self.pygame.draw.rect(self.surf, ORANGE, [self.padding, 0, 80, 16], 0)
        
        self.pygame.draw.rect(self.surf, WHITE, [self.padding, 30, self.width, 5], 0)

        self.surf.blit(self.txt_surf, self.txt_rect)  # this surface never changes

        # dynamic graphics - button surface #
        self.button_surf = self.pygame.surface.Surface((30, 30))
        self.button_surf.fill(TRANS)
        self.button_surf.set_colorkey(TRANS)
#         pygame.draw.circle(self.button_surf, BLACK, (10, 10), 8, 0)
        self.pygame.draw.circle(self.button_surf, ORANGE, (15, 15), 10, 0)

    def draw(self):
        """ Combination of static and dynamic graphics in a copy of
    the basic slide surface
    """
        # static
        surf = self.surf.copy()

        # dynamic
        pos = (self.padding + int((self.val-self.mini)/(self.maxi-self.mini)*self.width), 33)
        self.button_rect = self.button_surf.get_rect(center=pos)
        
        self.txt_surf = self.font.render(str(self.val), 1, BLACK)
        self.txt_rect = self.txt_surf.get_rect(center=pos)
        surf.blit(self.button_surf, self.button_rect)
        surf.blit(self.txt_surf, self.txt_rect)
        self.button_rect.move_ip(self.xpos, self.ypos)  # move of button box to correct screen position

        # screen
        self.screen.blit(surf, (self.xpos, self.ypos))

    def move(self):
        """
    The dynamic part; reacts to movement of the slider button.
    """
        self.val = (self.pygame.mouse.get_pos()[0] - self.xpos - self.padding) / self.width * (self.maxi - self.mini) + self.mini
        if self.val < self.mini:
            self.val = self.mini
        if self.val > self.maxi:
            self.val = self.maxi
        self.val = int(self.val)
        self.callback(self.val)
    
    def set_value(self, val):
        self.val = val

class Button():
    def __init__(self, pygame, screen, txt, location, action, bg=WHITE, fg=BLACK, size=(80, 30), font_name="Segoe Print", font_size=16):
        self.pygame = pygame
        self.screen = screen
        self.color = bg  # the static (normal) color
        self.bg = bg  # actual background color, can change on mouseover
        self.fg = fg  # text color
        self.size = size

        self.font = self.pygame.font.SysFont(font_name, font_size)
        self.txt = txt
        self.txt_surf = self.font.render(self.txt, 1, self.fg)
        self.txt_rect = self.txt_surf.get_rect(center=[s//2 for s in self.size])

        self.surface = self.pygame.surface.Surface(size)
        self.rect = self.surface.get_rect(center=location)

        self.call_back_ = action

    def draw(self):
        self.mouseover()

        self.surface.fill(self.bg)
        self.surface.blit(self.txt_surf, self.txt_rect)
        self.screen.blit(self.surface, self.rect)

    def mouseover(self):
        self.bg = self.color
        pos = self.pygame.mouse.get_pos()
        if self.rect.collidepoint(pos):
            self.bg = GREY  # mouseover color

    def call_back(self):
        self.call_back_()

# class PreviewScreen():
#     def __init__(self, pygame, screen):
#         self.pygame = pygame
#         self.screen = screen
    
#     def draw():

import numpy as np

class DrawWindow():
    background_surface = None
    overlay_surface = None
    mouse_button_left_handler = None
    draw_on = False
    pan_on = False
    background_on = True
    button_draw_enabled = False
    button_erase_enabled = False
    button_pan_enabled = False

    last_pos = (0, 0)
    color = (255, 0, 0)
    eraser = (0, 0, 0)
    draw_color = color
    radius = PIXEL_RADIUS_DEFAULT
    origin = (0, 0)
    current_position = (0, 0)
    
    def __init__(self, pygame, screen, background_surface, overlay_surface, size=(800,800)):
        self.pygame = pygame
        self.screen = screen
        self.cursor_surface = pygame.Surface((size[0], size[1]), pygame.SRCALPHA)
        self.set_surfaces(background_surface, overlay_surface)
        self.size = size
        self.mouse_button_left_handler = self.handle_draw
        
    def set_surfaces(self, background_surface, overlay_surface):
        self.original_surface = background_surface
        self.background_surface = background_surface
        self.overlay_surface = overlay_surface
        self.black_background = self.pygame.Surface((self.background_surface.get_width(), self.background_surface.get_height()))
        
    def draw(self):
        self.screen.blits(
            (
                (self.background_surface, (0,0), (-self.current_position[0], -self.current_position[1], self.size[0], self.size[1])),
                (self.overlay_surface, (0,0), (-self.current_position[0], -self.current_position[1], self.size[0], self.size[1]))
            )
        )

    def roundline(self, srf, color, start, end, radius=1):
        dx = end[0]-start[0]
        dy = end[1]-start[1]
        distance = max(abs(dx), abs(dy))
        for i in range(distance):
            x = int( start[0]+float(i)/distance*dx)
            y = int( start[1]+float(i)/distance*dy)
            self.pygame.draw.circle(srf, color, (x, y), radius)

    def draw_cursor(self, mouse_position):
        self.pygame.draw.circle(self.cursor_surface, (0, 0, 255, 100), mouse_position, self.radius)
        self.screen.blit(self.cursor_surface, (0,0), (0, 0, self.size[0], self.size[1]))
        self.cursor_surface.fill((0, 0, 0, 0))
    
    def handle_draw(self, mouse_position):
        self.draw_on = True
        self.draw_color = self.color
        cursor_offset = tuple(np.subtract(mouse_position, self.current_position))
        self.pygame.draw.circle(self.overlay_surface, self.draw_color, cursor_offset, self.radius)

    def handle_erase(self, mouse_position):
        self.draw_on = True
        self.draw_color = self.eraser
        cursor_offset = tuple(np.subtract(mouse_position, self.current_position))
        self.pygame.draw.circle(self.overlay_surface, self.draw_color, cursor_offset, self.radius)

    def handle_pan(self, mouse_position):
        self.pan_on = True
        self.prev_position = mouse_position

    def handle_brush_up(self, mouse_position, callback):
        if self.radius < PIXEL_RADIUS_MAX and self.radius:
            self.radius += 1
            self.draw_cursor(mouse_position)
            callback(self.radius)

    def handle_brush_down(self, mouse_position, callback):
        if self.radius > PIXEL_RADIUS_MIN:
            self.radius -= 1
            self.draw_cursor(mouse_position)
            callback(self.radius)
    
    def handle_mouse_move(self, mouse_position):
        cursor_offset = tuple(np.subtract(mouse_position, self.current_position))

        if self.pan_on:
            pos_diff = np.subtract(mouse_position, self.prev_position)
            temp_position = tuple(np.add(self.current_position, pos_diff))
            # left x boundary
            if temp_position[0] > 0:
                temp_position = (0, temp_position[1])
            # right x boundary
            if np.abs(temp_position[0]) + self.size[0] > self.background_surface.get_width():
                temp_position = (-(self.background_surface.get_width() - self.size[0]), temp_position[1])
            # upper y boundary
            if temp_position[1] > 0:
                temp_position = (temp_position[0], 0)
            # lower y boundary
            if np.abs(temp_position[1]) + self.size[1] > self.background_surface.get_height():
                temp_position = (temp_position[0], -(self.background_surface.get_height() - self.size[1]))
            self.current_position = temp_position

            self.prev_position = mouse_position
        if self.draw_on:
            self.pygame.draw.circle(self.overlay_surface, self.draw_color, cursor_offset, self.radius)
            self.roundline(self.overlay_surface, self.draw_color, cursor_offset, self.last_pos,  self.radius)

        self.last_pos = cursor_offset
        if not self.pan_on:
            self.draw_cursor(mouse_position)
        
    def handle_mouse_button_up(self, mouse_position):
        self.disable_tools()
        self.draw_cursor(mouse_position)
        
    def handle_background_toggle(self):
        self.background_on = not self.background_on
        if self.background_on:
            self.background_surface = self.original_surface
        else:
            self.background_surface = self.black_background
            
    def disable_tools(self):
        self.draw_on = False
        self.pan_on = False
        
    def set_radius(self, radius):
        self.radius = radius