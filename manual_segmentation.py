import sys
from skimage.io import imread, imsave
import numpy as np
import pygame
from pygame.locals import HWSURFACE, DOUBLEBUF, RESIZABLE, VIDEORESIZE
from components import Slider, Button, DrawWindow

pygame.init()

PIXEL_RADIUS_DEFAULT = 2
PIXEL_RADIUS_MIN = 1
PIXEL_RADIUS_MAX = 20
# SIDEBAR_WIDTH = PREVIEW_WIDTH

transparent_color = (0,0,0)

def run(slice_image_names, pred_image_names):
    # load slices into ndarrays with 3 color channels
    slice_images = [imread(n).T for n in slice_image_names]
    slice_images = [np.dstack((image,image,image)) for image in slice_images]

    pred_images = [imread(n).T for n in pred_image_names]
    for image in pred_images:
        image[image >= 100] = 255
        image[image < 100] = 0
    empty_slice = np.zeros((pred_images[0].shape[0], pred_images[0].shape[1]))
    pred_images = [np.dstack((image,empty_slice,empty_slice)) for image in pred_images]

    # turn the slices into surfaces
    slice_surfaces = [pygame.surfarray.make_surface(image) for image in slice_images]
    pred_surfaces = [pygame.surfarray.make_surface(image) for image in pred_images]
    for surface in pred_surfaces:
        surface.set_colorkey(transparent_color)

    current_slice_surface = slice_surfaces[0]
    current_pred_surface = pred_surfaces[0]

    # screen width will be dependant on the width of the original image
    IMAGE_WIDTH = 800
    IMAGE_HEIGHT = 800
    PREVIEW_HEIGHT = int(current_slice_surface.get_height() * .10)
    PREVIEW_WIDTH = int(current_slice_surface.get_width() * .10)
    PREVIEW_LOCATION_WIDTH = IMAGE_WIDTH * .10
    PREVIEW_LOCATION_HEIGHT = IMAGE_HEIGHT * .10
    SCREEN_WIDTH = IMAGE_WIDTH + PREVIEW_WIDTH
    SCREEN_HEIGHT = 800

    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT),HWSURFACE|DOUBLEBUF) #|RESIZABLE)


    preview_screen = pygame.transform.scale(current_slice_surface, (PREVIEW_WIDTH, PREVIEW_HEIGHT))
    preview_screen.blit(pygame.transform.scale(current_pred_surface, (PREVIEW_WIDTH, PREVIEW_HEIGHT)), (0,0))
    preview_screen_location = pygame.Surface((PREVIEW_WIDTH, PREVIEW_HEIGHT), pygame.SRCALPHA)

    draw_window = DrawWindow(pygame, screen, current_slice_surface, current_pred_surface)

    def draw_preview_location():
        current_position = draw_window.current_position
        preview_screen.blits((
            (pygame.transform.scale(draw_window.background_surface, (PREVIEW_WIDTH, PREVIEW_HEIGHT)), (0,0)),
            (pygame.transform.scale(draw_window.overlay_surface, (PREVIEW_WIDTH, PREVIEW_HEIGHT)), (0,0))
        ))
        pygame.draw.rect(preview_screen_location, (0, 0, 255, 100), (-current_position[0] * .10, -current_position[1] * .10, PREVIEW_LOCATION_WIDTH, PREVIEW_LOCATION_HEIGHT))
        preview_screen.blit(preview_screen_location, (0, 0), (0, 0, PREVIEW_WIDTH, PREVIEW_HEIGHT))
        preview_screen_location.fill((0, 0, 0, 0))
        screen.blit(preview_screen, (IMAGE_WIDTH, SCREEN_HEIGHT - PREVIEW_HEIGHT))

    def handle_draw_button():
    #     draw_window.disable_tools()
        draw_window.mouse_button_left_handler = draw_window.handle_draw

    def handle_erase_button():
    #     draw_window.disable_tools()
        draw_window.mouse_button_left_handler = draw_window.handle_erase

    def handle_pan_button():
    #     draw_window.disable_tools()
        draw_window.mouse_button_left_handler = draw_window.handle_pan

    def handle_slice_slider(index):
        draw_window.set_surfaces(slice_surfaces[index-1], pred_surfaces[index-1])
        draw_preview_location()


    slice_slider = Slider(pygame,
                         screen,
                         'current slice',
                         1,
                         len(slice_surfaces),
                         1,
                         (IMAGE_WIDTH + 30, 10),
                         handle_slice_slider)

    button_draw = Button(pygame, screen, 'draw', (IMAGE_WIDTH + 80, 80), handle_draw_button)
    button_erase = Button(pygame, screen, 'erase', (IMAGE_WIDTH + (80 * 2) + 5, 80), handle_erase_button)
    button_pan = Button(pygame, screen, 'pan', (IMAGE_WIDTH + (80 * 3) + 10, 80), handle_pan_button)


    pixel_radius_slider = Slider(pygame, 
                                 screen, 
                                 'pixel radius', 
                                 PIXEL_RADIUS_DEFAULT, 
                                 PIXEL_RADIUS_MAX, 
                                 PIXEL_RADIUS_MIN, 
                                 (IMAGE_WIDTH + 30, 110), draw_window.set_radius)
    slides = [slice_slider, pixel_radius_slider]
    buttons = [button_draw, button_erase, button_pan]


    # draw preview at least once before the main loop
    draw_preview_location()

    running = True
    while running:
        current_position = draw_window.current_position
        for button in buttons:

            button.draw()
        # Move slides
        for s in slides:
            if s.hit:
                s.move()
        for s in slides:
            s.draw()

        draw_window.draw()

        e = pygame.event.wait()

        pos = pygame.mouse.get_pos()

        # events for main image window
        if pos[0] <= IMAGE_WIDTH:
            if e.type == pygame.MOUSEBUTTONDOWN:
                if e.button == pygame.BUTTON_WHEELUP:
                    draw_window.handle_brush_up(pos, pixel_radius_slider.set_value)
                elif e.button == pygame.BUTTON_WHEELDOWN:
                    draw_window.handle_brush_down(pos, pixel_radius_slider.set_value)
                elif e.button == pygame.BUTTON_MIDDLE:
                    draw_window.handle_pan(pos)
                else:
                    if e.button == pygame.BUTTON_LEFT:
                        draw_window.mouse_button_left_handler(pos)
                    if e.button == pygame.BUTTON_RIGHT:
                        draw_window.handle_erase(pos)

    #                 draw_window.draw_on = True
            elif e.type == pygame.MOUSEBUTTONUP:
                for s in slides:
                    s.hit = False
                draw_window.handle_mouse_button_up(pos)
                draw_preview_location()

            elif e.type == pygame.MOUSEMOTION: 
                draw_window.handle_mouse_move(pos)
                if draw_window.pan_on:
                    draw_preview_location()

        # events for sidebar
        else:
            if e.type == pygame.MOUSEBUTTONUP:
                draw_window.handle_mouse_button_up(pos)
                for s in slides:
                    s.hit = False
            if e.type == pygame.MOUSEBUTTONDOWN:
                for button in buttons:
                    if button.rect.collidepoint(pos):
                        button.call_back()
                for s in slides:
                    if s.button_rect.collidepoint(pos):
                        s.hit = True



        if e.type == pygame.KEYDOWN:
            if e.key == pygame.K_b:
                draw_window.handle_background_toggle()
                draw_preview_location()
            if e.key == pygame.K_s:
                imsave('test.png', pygame.surfarray.pixels_red(predicted_slice_surface).T)
            if e.key == pygame.K_ESCAPE:
                running = False
        if e.type == pygame.QUIT:
                running = False
        pygame.display.flip()


    pygame.quit()
