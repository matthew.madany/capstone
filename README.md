Model Eval Comments:
https://github.com/junyanz/CycleGAN/issues/94#issuecomment-395936730
https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/issues/166#issuecomment-354511534
https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/issues/166#issuecomment-510933308
https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/issues/166#issuecomment-511475469

Links to Data Download:

Cerebellum Sample: https://nextcloud.nautilus.optiputer.net/s/MLRXBWpjFM3Amji/download

Cortex1 Sample: https://nextcloud.nautilus.optiputer.net/s/SkeysmqwfbgBNQM/download

Cortex2 Sample: https://nextcloud.nautilus.optiputer.net/s/Ya7JR5jJbyKs3Ak/download

Hypothalamus Sample: https://nextcloud.nautilus.optiputer.net/s/qwgCqknTi3zEaLs/download

dat1.zip, Example em_roi with Mito and Membrane Predictions: https://nextcloud.nautilus.optiputer.net/s/BEATQWX4q4pfDNZ/download

Label HDF5 Volume: https://nextcloud.nautilus.optiputer.net/s/cdc78DBxNycTSzF/download

```
[madany@famine dscapstone]$ wget https://nextcloud.nautilus.optiputer.net/s/MLRXBWpjFM3Amji/download
--2020-02-02 17:22:28--  https://nextcloud.nautilus.optiputer.net/s/MLRXBWpjFM3Amji/download
Resolving nextcloud.nautilus.optiputer.net (nextcloud.nautilus.optiputer.net)... 198.17.101.46, 128.114.109.76, 130.191.103.1, ...
Connecting to nextcloud.nautilus.optiputer.net (nextcloud.nautilus.optiputer.net)|198.17.101.46|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 2921641283 (2.7G) [application/zip]
Saving to: ‘download’

100%[===========================================================================================================================================================================================================================================================================================================>] 2,921,641,283 45.7MB/s   in 70s    

2020-02-02 17:23:52 (39.9 MB/s) - ‘download’ saved [2921641283/2921641283]

[madany@famine dscapstone]$ ls
download
[madany@famine dscapstone]$ unzip download
Archive:  download
  inflating: Cerebellum/slice0000.png  
  inflating: Cerebellum/slice0001.png  
  inflating: Cerebellum/slice0002.png  
  inflating: Cerebellum/slice0003.png  
  inflating: Cerebellum/slice0004.png  
  inflating: Cerebellum/slice0005.png  
  inflating: Cerebellum/slice0006.png  
  inflating: Cerebellum/slice0007.png  
  inflating: Cerebellum/slice0008.png  
  inflating: Cerebellum/slice0009.png  
  inflating: Cerebellum/slice0010.png  
  inflating: Cerebellum/slice0011.png  
  inflating: Cerebellum/slice0012.png  
  inflating: Cerebellum/slice0013.png  
  inflating: Cerebellum/slice0014.png  
  inflating: Cerebellum/slice0015.png  
  inflating: Cerebellum/slice0016.png  
  inflating: Cerebellum/slice0017.png  
  inflating: Cerebellum/slice0018.png  
  inflating: Cerebellum/slice0019.png  
  inflating: Cerebellum/slice0020.png  
  inflating: Cerebellum/slice0021.png  
  inflating: Cerebellum/slice0022.png  
  inflating: Cerebellum/slice0023.png  
  inflating: Cerebellum/slice0024.png  
  inflating: Cerebellum/slice0025.png  
  inflating: Cerebellum/slice0026.png  
  inflating: Cerebellum/slice0027.png  
  inflating: Cerebellum/slice0028.png  
  inflating: Cerebellum/slice0029.png  
  inflating: Cerebellum/slice0030.png  
  inflating: Cerebellum/slice0031.png  
  inflating: Cerebellum/slice0032.png  
  inflating: Cerebellum/slice0033.png  
  inflating: Cerebellum/slice0034.png  
  inflating: Cerebellum/slice0035.png  
  inflating: Cerebellum/slice0036.png  
  inflating: Cerebellum/slice0037.png  
  inflating: Cerebellum/slice0038.png  
  inflating: Cerebellum/slice0039.png  
  inflating: Cerebellum/slice0040.png  
  inflating: Cerebellum/slice0041.png  
  inflating: Cerebellum/slice0042.png  
  inflating: Cerebellum/slice0043.png  
  inflating: Cerebellum/slice0044.png  
  inflating: Cerebellum/slice0045.png  
  inflating: Cerebellum/slice0046.png  
  inflating: Cerebellum/slice0047.png  
  inflating: Cerebellum/slice0048.png  
  inflating: Cerebellum/slice0049.png  
  inflating: Cerebellum/slice0050.png  
  inflating: Cerebellum/slice0051.png  
  inflating: Cerebellum/slice0052.png  
  inflating: Cerebellum/slice0053.png  
  inflating: Cerebellum/slice0054.png  
  inflating: Cerebellum/slice0055.png  
  inflating: Cerebellum/slice0056.png  
  inflating: Cerebellum/slice0057.png  
  inflating: Cerebellum/slice0058.png  
  inflating: Cerebellum/slice0059.png  
  inflating: Cerebellum/slice0060.png  
  inflating: Cerebellum/slice0061.png  
  inflating: Cerebellum/slice0062.png  
  inflating: Cerebellum/slice0063.png  
  inflating: Cerebellum/slice0064.png  
  inflating: Cerebellum/slice0065.png  
  inflating: Cerebellum/slice0066.png  
  inflating: Cerebellum/slice0067.png  
  inflating: Cerebellum/slice0068.png  
  inflating: Cerebellum/slice0069.png  
  inflating: Cerebellum/slice0070.png  
  inflating: Cerebellum/slice0071.png  
  inflating: Cerebellum/slice0072.png  
  inflating: Cerebellum/slice0073.png  
  inflating: Cerebellum/slice0074.png  
  inflating: Cerebellum/slice0075.png  
  inflating: Cerebellum/slice0076.png  
  inflating: Cerebellum/slice0077.png  
  inflating: Cerebellum/slice0078.png  
  inflating: Cerebellum/slice0079.png  
  inflating: Cerebellum/slice0080.png  
  inflating: Cerebellum/slice0081.png  
  inflating: Cerebellum/slice0082.png  
  inflating: Cerebellum/slice0083.png  
  inflating: Cerebellum/slice0084.png  
  inflating: Cerebellum/slice0085.png  
  inflating: Cerebellum/slice0086.png  
  inflating: Cerebellum/slice0087.png  
  inflating: Cerebellum/slice0088.png  
  inflating: Cerebellum/slice0089.png  
  inflating: Cerebellum/slice0090.png  
  inflating: Cerebellum/slice0091.png  
  inflating: Cerebellum/slice0092.png  
  inflating: Cerebellum/slice0093.png  
  inflating: Cerebellum/slice0094.png  
  inflating: Cerebellum/slice0095.png  
  inflating: Cerebellum/slice0096.png  
  inflating: Cerebellum/slice0097.png  
  inflating: Cerebellum/slice0098.png  
  inflating: Cerebellum/slice0099.png  
  inflating: Cerebellum/slice0100.png  
  inflating: Cerebellum/slice0101.png  
  inflating: Cerebellum/slice0102.png  
  inflating: Cerebellum/slice0103.png  
  inflating: Cerebellum/slice0104.png  
  inflating: Cerebellum/slice0105.png  
  inflating: Cerebellum/slice0106.png  
  inflating: Cerebellum/slice0107.png  
  inflating: Cerebellum/slice0108.png  
  inflating: Cerebellum/slice0109.png  
  inflating: Cerebellum/slice0110.png  
  inflating: Cerebellum/slice0111.png  
  inflating: Cerebellum/slice0112.png  
  inflating: Cerebellum/slice0113.png  
  inflating: Cerebellum/slice0114.png  
  inflating: Cerebellum/slice0115.png  
  inflating: Cerebellum/slice0116.png  
  inflating: Cerebellum/slice0117.png  
  inflating: Cerebellum/slice0118.png  
  inflating: Cerebellum/slice0119.png  
  inflating: Cerebellum/slice0120.png  
  inflating: Cerebellum/slice0121.png  
  inflating: Cerebellum/slice0122.png  
  inflating: Cerebellum/slice0123.png  
  inflating: Cerebellum/slice0124.png  
  inflating: Cerebellum/slice0125.png  
  inflating: Cerebellum/slice0126.png  
  inflating: Cerebellum/slice0127.png  
  inflating: Cerebellum/slice0128.png  
  inflating: Cerebellum/slice0129.png  
  inflating: Cerebellum/slice0130.png  
  inflating: Cerebellum/slice0131.png  
  inflating: Cerebellum/slice0132.png  
  inflating: Cerebellum/slice0133.png  
  inflating: Cerebellum/slice0134.png  
  inflating: Cerebellum/slice0135.png  
  inflating: Cerebellum/slice0136.png  
  inflating: Cerebellum/slice0137.png  
  inflating: Cerebellum/slice0138.png  
  inflating: Cerebellum/slice0139.png  
  inflating: Cerebellum/slice0140.png  
  inflating: Cerebellum/slice0141.png  
  inflating: Cerebellum/slice0142.png  
  inflating: Cerebellum/slice0143.png  
  inflating: Cerebellum/slice0144.png  
  inflating: Cerebellum/slice0145.png  
  inflating: Cerebellum/slice0146.png  
  inflating: Cerebellum/slice0147.png  
  inflating: Cerebellum/slice0148.png  
  inflating: Cerebellum/slice0149.png  
  inflating: Cerebellum/slice0150.png  
  inflating: Cerebellum/slice0151.png  
  inflating: Cerebellum/slice0152.png  
  inflating: Cerebellum/slice0153.png  
  inflating: Cerebellum/slice0154.png  
  inflating: Cerebellum/slice0155.png  
  inflating: Cerebellum/slice0156.png  
  inflating: Cerebellum/slice0157.png  
  inflating: Cerebellum/slice0158.png  
  inflating: Cerebellum/slice0159.png  
  inflating: Cerebellum/slice0160.png  
  inflating: Cerebellum/slice0161.png  
  inflating: Cerebellum/slice0162.png  
  inflating: Cerebellum/slice0163.png  
  inflating: Cerebellum/slice0164.png  
  inflating: Cerebellum/slice0165.png  
  inflating: Cerebellum/slice0166.png  
  inflating: Cerebellum/slice0167.png  
  inflating: Cerebellum/slice0168.png  
  inflating: Cerebellum/slice0169.png  
  inflating: Cerebellum/slice0170.png  
  inflating: Cerebellum/slice0171.png  
  inflating: Cerebellum/slice0172.png  
  inflating: Cerebellum/slice0173.png  
  inflating: Cerebellum/slice0174.png  
  inflating: Cerebellum/slice0175.png  
  inflating: Cerebellum/slice0176.png  
  inflating: Cerebellum/slice0177.png  
  inflating: Cerebellum/slice0178.png  
  inflating: Cerebellum/slice0179.png  
  inflating: Cerebellum/slice0180.png  
  inflating: Cerebellum/slice0181.png  
  inflating: Cerebellum/slice0182.png  
  inflating: Cerebellum/slice0183.png  
  inflating: Cerebellum/slice0184.png  
  inflating: Cerebellum/slice0185.png  
  inflating: Cerebellum/slice0186.png  
  inflating: Cerebellum/slice0187.png  
  inflating: Cerebellum/slice0188.png  
  inflating: Cerebellum/slice0189.png  
  inflating: Cerebellum/slice0190.png  
  inflating: Cerebellum/slice0191.png  
  inflating: Cerebellum/slice0192.png  
  inflating: Cerebellum/slice0193.png  
  inflating: Cerebellum/slice0194.png  
  inflating: Cerebellum/slice0195.png  
  inflating: Cerebellum/slice0196.png  
  inflating: Cerebellum/slice0197.png  
  inflating: Cerebellum/slice0198.png  
  inflating: Cerebellum/slice0199.png  
  inflating: Cerebellum/slice0200.png  
  inflating: Cerebellum/slice0201.png  
  inflating: Cerebellum/slice0202.png  
  inflating: Cerebellum/slice0203.png  
  inflating: Cerebellum/slice0204.png  
  inflating: Cerebellum/slice0205.png  
  inflating: Cerebellum/slice0206.png  
  inflating: Cerebellum/slice0207.png  
  inflating: Cerebellum/slice0208.png  
  inflating: Cerebellum/slice0209.png  
  inflating: Cerebellum/slice0210.png  
  inflating: Cerebellum/slice0211.png  
  inflating: Cerebellum/slice0212.png  
  inflating: Cerebellum/slice0213.png  
  inflating: Cerebellum/slice0214.png  
  inflating: Cerebellum/slice0215.png  
  inflating: Cerebellum/slice0216.png  
  inflating: Cerebellum/slice0217.png  
  inflating: Cerebellum/slice0218.png  
  inflating: Cerebellum/slice0219.png  
  inflating: Cerebellum/slice0220.png  
  inflating: Cerebellum/slice0221.png  
  inflating: Cerebellum/slice0222.png  
  inflating: Cerebellum/slice0223.png  
  inflating: Cerebellum/slice0224.png  
  inflating: Cerebellum/slice0225.png  
  inflating: Cerebellum/slice0226.png  
  inflating: Cerebellum/slice0227.png  
  inflating: Cerebellum/slice0228.png  
  inflating: Cerebellum/slice0229.png  
  inflating: Cerebellum/slice0230.png  
  inflating: Cerebellum/slice0231.png  
  inflating: Cerebellum/slice0232.png  
  inflating: Cerebellum/slice0233.png  
  inflating: Cerebellum/slice0234.png  
  inflating: Cerebellum/slice0235.png  
  inflating: Cerebellum/slice0236.png  
  inflating: Cerebellum/slice0237.png  
  inflating: Cerebellum/slice0238.png  
  inflating: Cerebellum/slice0239.png  
  inflating: Cerebellum/slice0240.png  
  inflating: Cerebellum/slice0241.png  
  inflating: Cerebellum/slice0242.png  
  inflating: Cerebellum/slice0243.png  
  inflating: Cerebellum/slice0244.png  
  inflating: Cerebellum/slice0245.png  
  inflating: Cerebellum/slice0246.png  
  inflating: Cerebellum/slice0247.png  
  inflating: Cerebellum/slice0248.png  
  inflating: Cerebellum/slice0249.png  
```



















