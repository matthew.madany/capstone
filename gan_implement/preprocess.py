import os
import skimage
import numpy as np
from PIL import Image
import math
from skimage.util import view_as_windows

tensorsize = 2600
overlap = 200

apxs1_imdir = #Directory storing test image 3d stack
writedir = #Directory to write preprocced images

imfiles = os.listdir(apxs1_imdir)
imfiles.sort()

#read in one image to get dims
im1 = Image.open(imfiles[0])

#round to nearest mutliple of tensorsize minus overlap and the ends dont' have overlap
szapp = tensorsize-overlap*2
sz = im1.shape
p0 = math.ceil(sz[0]/szapp)
p1 = math.ceil(sz[1]/szapp)
s0 = p0*szapp+overlap*2
s1 = p1*szapp+overlap*2

#pad with mirrored pixel values
pad0 = int((s0-sz[0])/2)
pad1 = int((s1-sz[1])/2)

padim = np.pad(im1,((pad0,),(pad1,)),'reflect')

#check if dims are odd
wodd = 0
hodd = 0
if padim.shape[0] != s0:
    hodd = 1
    padim = np.pad(padim,((0,1),(0,0)),'reflect')
if padim.shape[1] != s1:
    wodd = 1
    padim = np.pad(padim,((0,0),(0,1)),'reflect')

#patch it 
K = view_as_windows(padim,(tensorsize, tensorsize),(szapp, szapp))

os.mkdir(writedir)
t = 0
for i in range(0,1000):
    K = skimage.io.imread(apxs1_imdir+'/'+imfiles[i])    
    padim = np.pad(K,((pad0,),(pad1,)),'reflect')
    if padim.shape[0] != s0:
        padim = np.pad(padim,((0,1),(0,0)),'reflect')
    if padim.shape[1] != s1:
        padim = np.pad(padim,((0,0),(0,1)),'reflect')
    K = view_as_windows(padim,(tensorsize, tensorsize),(szapp, szapp))
    for p in range(0,K.shape[0]):
        for q in range(0, K.shape[1]):
            sim = Image.fromarray(K[p,q,:,:])
            sim.save(writedir + '/' + 'image_%05d.jpg' % t)
            sim.close()
            t += 1

#don't leave file handlers open with PIL
im1.close()

#example command on resulting directory
#python3.5 test.py --model test --dataset_mode single --dataroot $folder/ganputs3 --checkpoints_dir $folder --name psapex_cyclegan_aligned --input_nc=1 --output_nc=1 --no_dropout --results_dir $folder/batchout --load_size 2600 --crop_size 2600
