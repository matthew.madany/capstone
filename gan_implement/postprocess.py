#Directory where test.py --results_dir outputed too
imdir =
#Output directory
writedir = 

##Variables from Preprocessing
#number of tiles that were processed
mxt = 
#number of images that were processed (how many image files in original input stack) 
nimg = 
#shape of input images
r = K.shape[0]*K.shape[1]

imfiles = os.listdir(imdir)
imfiles.sort()
ims = [imdir + '/' + s for s in imfiles]

#just want the images with 'fake' in the name
matching = [s for s in ims if "fake" in s]

for i in range(0,nimg):
f = np.asarray(range(0,r))+i*r
    G = np.empty((K.shape))
    imr = 0+min(f)
     #merge tiles with overlap blending
    for p in range(0,K.shape[0]):
        for q in range(0, K.shape[1]):
            T = skimage.io.imread(matching[imr])
            T = np.mean(T,axis=2)
            G[p,q] = T
            imr += 1
    rows = {}
    for p in range(0,K.shape[0]):
        firstim = G[p,0]
        for q in range(1, K.shape[1]):
            rowbind1 = np.concatenate((firstim,np.zeros((firstim.shape[0],G[p,q].shape[1]-overlap*2))),axis=1)
            rowbind2 = np.concatenate((np.zeros((firstim.shape[0],rowbind1.shape[1]-G[p,q].shape[1])),G[p,q]),axis=1)
            row3 = np.zeros(rowbind1.shape)
            row3 = np.dstack((rowbind1,rowbind2,row3))
            row3 = row3.astype('double').astype('uint8')
            #plt.imshow(row3)#[1400:2200,2000:2800,:], interpolation='nearest')
            #plt.show()
            #blend grandinet
            #scalar to identiy overlap region that is blended, for example .25 means inner 25% of pixels are blended
            blend = .25
            #build a single dimenstion array to multiple a gradient of blend values
            grad = np.asarray(range(0,overlap*2,round(blend/2*overlap*2)))
            middle = int(len(grad) / 2)
            lb = grad[middle-1]
            rb = grad[middle]
            grad = np.concatenate((np.zeros(round((.5-blend/2)*overlap*2)),np.asarray(range(0,rb-lb))/(rb-lb)),axis=0)
            grad = np.concatenate((grad,np.ones(overlap*2-len(grad))))
            #inverse array to be applied to the 2nd image
            invgrad = np.absolute(grad.astype('double')-1)
            #apply gradient to first overlap
            row3[:,row3.shape[1]-G[p,q].shape[1]:row3.shape[1]-G[p,q].shape[1]+overlap*2,0]= \
                    row3[:,row3.shape[1]-G[p,q].shape[1]:row3.shape[1]-G[p,q].shape[1]+overlap*2,0]*invgrad
            #second overlap
            row3[:,row3.shape[1]-G[p,q].shape[1]:row3.shape[1]-G[p,q].shape[1]+overlap*2,1]=  \
                    row3[:,row3.shape[1]-G[p,q].shape[1]:row3.shape[1]-G[p,q].shape[1]+overlap*2,1]*grad
            #plt.imshow(row3[1400:2200,2000:2800,:], interpolation='nearest')
            #plt.show()
            sumimage = np.sum(row3.astype('double'),axis=2).astype('uint8')
            #plt.imshow(sumimage[1400:2200,2000:2800], interpolation='nearest',cmap='gray')
            #plt.show()
            #plt.imshow(sumimage, interpolation='nearest',cmap='gray')
            #plt.show()
            firstim = sumimage
        rows[p] = firstim
        
    #blend rows
    firstim = rows[0]
    for p in range(1,len(rows)):
        row = rows[p]
        colbind1 = np.concatenate((firstim,np.zeros((row.shape[0]-overlap*2,row.shape[1]))),axis=0)
        colbind2 = np.concatenate((np.zeros((colbind1.shape[0]-row.shape[0],row.shape[1])),row),axis=0)
        col3 = np.zeros(colbind1.shape)
        col3 = np.dstack((colbind1,colbind2,col3))
        col3 = col3.astype('double').astype('uint8')
        #plt.imshow(col3)#[1400:2200,2000:2800,:], interpolation='nearest')
        #plt.show()
        #gradients along new axis
        vgrad = np.transpose(np.atleast_2d(grad))
        vinvgrad = np.transpose(np.atleast_2d(invgrad))
        #apply blend
        col3[col3.shape[0]-row.shape[0]:col3.shape[0]-row.shape[0]+overlap*2,:,0]= \
                    col3[col3.shape[0]-row.shape[0]:col3.shape[0]-row.shape[0]+overlap*2,:,0]*vinvgrad
        col3[col3.shape[0]-row.shape[0]:col3.shape[0]-row.shape[0]+overlap*2,:,1]= \
                    col3[col3.shape[0]-row.shape[0]:col3.shape[0]-row.shape[0]+overlap*2,:,1]*vgrad
        #plt.imshow(col3, interpolation='nearest')
        #plt.show()
        
        sumimage = np.sum(col3.astype('double'),axis=2).astype('uint8')
        #plt.imshow(sumimage, interpolation='nearest',cmap='gray')
        #plt.show()
        firstim=sumimage
    fimage = firstim
    #de pad
    depad = fimage[pad0:-pad0,pad1:-pad1]
    if hodd==1:
        depad = depad[0:-1,:]
    if wodd==1:
        depad = depad[:,0:-1]
    sim = Image.fromarray(depad)
    sim.save(writedir + '/' + 'image_%05d.tif' % i)
    sim.close()
