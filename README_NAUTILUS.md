
1. You should use this link: ncmirhub.nautilus.optiputer.net
2. Ask for 2 GPUS, 4 cores, and 8gb RAM.
3. Download the new EM_Analysis_Nautilus notebook from the repo and upload it to your lab.
4. There's 3 main storage directories:
 - /home/jovyan is your storage for scripts, notebook updates, new notebooks, python files, and smaller datasets, it is persistent and will always be there even when you restart your server.
 - /dev/shm is what's called an ephemeral disk storage, it's a drive that's been mapped to the node's RAM disk and is for rapid I/O operations, files here will clear when you restart the server, and is where large temp files should be stored.
 - /cephfs/jupyter is the shared filesystem where we'll be able to store input data, intermediate data, and final results as well as any data to be transferred between us, it gets about 20-60 mpbs read/write depending on the day and cluster usage, you should first create a folder here with your name when you first boot up your server.
5. Open a console and do mkdir /cephfs/jupyter/<YOURNAME> then replace instances where I use Matthew in the notebook with your name.

NOTE: 
1. When you're spawning, check both boxes and specify the National Center for Microscopy Neural Network Playground as your image.
2. Make sure to close the server 'file>hub control panel' when you're done, this will end your instance and free up the resources.